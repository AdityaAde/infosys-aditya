import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ElipticalButtonWidget extends StatelessWidget {
  const ElipticalButtonWidget({
    super.key,
    required this.widget,
    this.onTap,
  });

  final Widget widget;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40.h,
      width: 110.w,
      child: ElevatedButton(
        onPressed: onTap,
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),
        child: widget,
      ),
    );
  }
}
