import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../component/theme/theme.dart';
import 'widgets.dart';

class ProductCardWidget extends StatelessWidget {
  const ProductCardWidget({
    super.key,
    this.urlImage,
    this.title,
    this.description,
    this.price = 0,
    this.stock,
  });

  final String? urlImage;
  final String? title;
  final String? description;
  final int price;
  final int? stock;

  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).textTheme;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
      child: Card(
        elevation: 10,
        child: Container(
          height: 150.h,
          width: double.infinity,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
          child: Row(
            children: [
              _imageProduct(),
              _productContent(textTheme),
            ],
          ),
        ),
      ),
    );
  }

  Expanded _productContent(TextTheme textTheme) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                Text(
                  title ?? '',
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: textTheme.titleMedium?.copyWith(color: AppColor.ink01),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  description ?? '',
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: textTheme.bodySmall?.copyWith(
                    color: AppColor.ink01.withOpacity(0.7),
                  ),
                ),
                SizedBox(height: 8.h),
                Text(
                  'Price: $price\$',
                  style: textTheme.bodySmall?.copyWith(
                    color: AppColor.ink01.withOpacity(0.7),
                  ),
                ),
                Text(
                  'Stock: $stock',
                  style: textTheme.bodySmall?.copyWith(
                    color: AppColor.ink01.withOpacity(0.7),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Expanded _imageProduct() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: ClipRRect(
            borderRadius: const BorderRadius.all(
              Radius.circular(10),
            ),
            child: ImageCachedWidget(url: urlImage ?? ''),
          ),
        ),
      ),
    );
  }
}
