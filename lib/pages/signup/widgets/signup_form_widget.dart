import 'package:flutter/material.dart';

import '../../../component/theme/theme.dart';
import '../../../widgets/widgets.dart';

class SignUpFormWidget extends StatelessWidget {
  const SignUpFormWidget({
    super.key,
    required this.title,
  });

  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: AppDimen.w16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Sign Up',
            style: AppStyle.materialTextStyle.headlineSmall
                ?.copyWith(fontWeight: FontWeight.w700),
          ),
          SizedBox(height: AppDimen.h12),
          TextFormField(
            decoration: const InputDecoration(
              labelText: 'User ID',
              hintText: 'User ID',
            ),
          ),
          TextFormField(
            decoration: const InputDecoration(
              labelText: 'Password',
              hintText: 'Password',
            ),
          ),
          SizedBox(height: AppDimen.h16),
          Align(
            alignment: Alignment.centerRight,
            child: ElipticalButtonWidget(
              widget: const Text(
                'Sign Up',
                style: TextStyle(fontWeight: FontWeight.w700),
              ),
              onTap: () {},
            ),
          ),
        ],
      ),
    );
  }
}
