import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../component/theme/theme.dart';
import '../../gen/assets.gen.dart';
import 'widgets/signup_form_widget.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({super.key});

  static const route = AutoRoute(
    path: '/signup',
    page: SignUpPage,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Assets.images.headerLogin.image(),
          AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: const Icon(
                Icons.arrow_back_ios,
                color: AppColor.white,
              ),
            ),
          ),
          const SignUpFormWidget(title: 'Sign Up')
        ],
      ),
    );
  }
}
