import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../component/component.dart';
import '../../../models/models.dart';
import '../../../repositories/repositories.dart';

part 'product_cubit.freezed.dart';
part 'product_state.dart';

class ProductCubit extends Cubit<ProductState> {
  final ProductRepository _productRepository;

  factory ProductCubit.create() => ProductCubit(getIt.get())..getListProduct();

  ProductCubit(this._productRepository) : super(const ProductState.initial());

  void getListProduct() async {
    emit(const ProductState.loading());
    final result = await _productRepository.getListProduct();
    result.fold(
      (l) => emit(ProductState.error(l.toString())),
      (r) => emit(ProductState.success(r)),
    );
  }
}
