import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../widgets/widgets.dart';
import '../cubit/product_cubit.dart';

class ProductListwidget extends StatelessWidget {
  const ProductListwidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductCubit, ProductState>(
      builder: (context, state) {
        return state.maybeWhen(
          orElse: () => const SizedBox.shrink(),
          loading: () => const Center(child: CircularProgressIndicator()),
          success: (products) => ListView.builder(
            itemCount: products.length,
            itemBuilder: (context, index) {
              final product = products[index];

              return ProductCardWidget(
                urlImage: product.thumbnail,
                title: product.title,
                description: product.description,
                price: product.price ?? 0,
                stock: product.stock ?? 0,
              );
            },
          ),
          error: (err) => const Center(child: Text('Something went wrong')),
        );
      },
    );
  }
}
