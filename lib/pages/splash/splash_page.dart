import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../component/route/routers.gr.dart';
import '../../gen/assets.gen.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  static const route = AutoRoute(
    path: '/splash',
    page: SplashPage,
    initial: true,
  );

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    Future.delayed(const Duration(seconds: 2), () {
      // After 2 seconds, navigate to the desired screen
      AutoRouter.of(context).replace(const LoginRoute());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Assets.images.headerSplash.image(),
          Assets.images.logo.image(),
          Assets.images.footerSplash.image(),
        ],
      ),
    );
  }
}
