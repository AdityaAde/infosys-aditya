import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../component/route/routers.gr.dart';
import 'bloc/cubit/login_cubit.dart';
import 'widgets/widgets.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  static const route = AutoRoute(
    path: '/login',
    page: LoginPage,
  );

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late final LoginCubit _loginCubit;

  @override
  void initState() {
    super.initState();
    _loginCubit = LoginCubit.create();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _loginCubit,
      child: BlocListener<LoginCubit, LoginState>(
        listener: (context, state) {
          state.maybeWhen(
            orElse: () {},
            success: (account) {
              Fluttertoast.showToast(
                toastLength: Toast.LENGTH_LONG,
                msg: 'Login Success, Hi, ${account.username}',
              );
              AutoRouter.of(context).replace(const HomeRoute());
            },
            error: (err) => Fluttertoast.showToast(
              msg: 'User ID or Password is incorrect $err',
            ),
          );
        },
        child: Scaffold(
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              HeaderHomeWidget(),
              LoginFormWidget(title: 'User ID'),
              SignUpWidget(),
            ],
          ),
        ),
      ),
    );
  }
}
