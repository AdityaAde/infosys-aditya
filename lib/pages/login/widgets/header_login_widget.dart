import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../gen/assets.gen.dart';

class HeaderHomeWidget extends StatelessWidget {
  const HeaderHomeWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Assets.images.headerLogin.image(),
        SizedBox(width: 23.w),
        Assets.images.logo.image(height: 50.h)
      ],
    );
  }
}
