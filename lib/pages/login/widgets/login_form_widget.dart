import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../component/theme/theme.dart';
import '../../../widgets/widgets.dart';
import '../bloc/cubit/login_cubit.dart';

class LoginFormWidget extends StatefulWidget {
  const LoginFormWidget({
    super.key,
    required this.title,
  });

  final String title;

  @override
  State<LoginFormWidget> createState() => _LoginFormWidgetState();
}

class _LoginFormWidgetState extends State<LoginFormWidget> {
  final _formKey = GlobalKey<FormState>();
  late final TextEditingController _usernameController;
  late final TextEditingController _passwordController;

  @override
  void initState() {
    super.initState();
    _usernameController = TextEditingController();
    _passwordController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    final loginCubit = context.read<LoginCubit>();
    return Form(
      key: _formKey,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: AppDimen.w16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Login',
              style: AppStyle.materialTextStyle.headlineSmall
                  ?.copyWith(fontWeight: FontWeight.w700),
            ),
            SizedBox(height: AppDimen.h10),
            const Text('Please sign in to continue'),
            SizedBox(height: AppDimen.h12),
            TextFormField(
              controller: _usernameController,
              decoration: InputDecoration(
                labelText: widget.title,
                hintText: widget.title,
              ),
              validator: (value) {
                if (value?.isEmpty ?? false) {
                  return 'Please enter username';
                }
                return null;
              },
            ),
            TextFormField(
              controller: _passwordController,
              decoration: const InputDecoration(
                labelText: 'Password',
                hintText: 'Password',
              ),
              obscureText: true,
              validator: (value) {
                if (value?.isEmpty ?? false) {
                  return 'Please enter an Password';
                }
                return null;
              },
            ),
            SizedBox(height: AppDimen.h16),
            BlocBuilder<LoginCubit, LoginState>(
              builder: (context, state) {
                return state.maybeWhen(
                  orElse: () => Align(
                    alignment: Alignment.centerRight,
                    child: ElipticalButtonWidget(
                      widget: const Text(
                        'Login',
                        style: TextStyle(fontWeight: FontWeight.w700),
                      ),
                      onTap: () {
                        if (_formKey.currentState?.validate() ?? false) {
                          loginCubit.login(
                            _usernameController.text.trim(),
                            _passwordController.text.trim(),
                          );
                        }
                      },
                    ),
                  ),
                  loading: () => Align(
                    alignment: Alignment.centerRight,
                    child: ElipticalButtonWidget(
                      widget: SizedBox(
                        height: 20.h,
                        width: 18.w,
                        child: const CircularProgressIndicator(
                          color: AppColor.white,
                          strokeWidth: 2,
                        ),
                      ),
                      onTap: () {},
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
