import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../component/route/routers.gr.dart';
import '../../../component/theme/theme.dart';

class SignUpWidget extends StatelessWidget {
  const SignUpWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: AppDimen.h14),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Dont\'t have an account?',
            style: AppStyle.materialTextStyle.bodyMedium,
          ),
          SizedBox(width: 4.w),
          InkWell(
            onTap: () => AutoRouter.of(context).push(const SignUpRoute()),
            child: Text(
              'Sign Up',
              style: AppStyle.materialTextStyle.bodyMedium
                  ?.copyWith(color: AppColor.orange),
            ),
          ),
        ],
      ),
    );
  }
}
