import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../component/component.dart';
import '../../../../models/models.dart';
import '../../../../repositories/repositories.dart';

part 'login_cubit.freezed.dart';
part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  final LoginRepository _loginRepository;

  factory LoginCubit.create() => LoginCubit(getIt.get());

  LoginCubit(this._loginRepository) : super(const LoginState.initial());

  void login(String id, String password) async {
    emit(const LoginState.loading());
    final result = await _loginRepository.login(id, password);

    result.fold(
      (err) => emit(LoginState.error(err.toString())),
      (account) => emit(LoginState.success(account)),
    );
  }
}
