abstract class Endpoint {
  String endpointBaseUrlWithVersion({
    required String path,
    String version = '',
  }) {
    return '/$version/$path';
  }
}
