import 'package:auto_route/auto_route.dart';

import '../../pages/pages.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    SplashPage.route,
    LoginPage.route,
    SignUpPage.route,
    HomePage.route,
  ],
)
class $AppRouter {}
