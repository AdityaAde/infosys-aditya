import 'dart:convert';

import '../../component/component.dart';
import '../../models/models.dart';
import 'base_service.dart';

class LoginService extends Endpoint {
  final BaseService _baseService;

  LoginService(this._baseService);

  factory LoginService.create() => LoginService(getIt.get());

  Future<AccountModels> login(String id, String password) async {
    final url = endpointBaseUrlWithVersion(path: 'auth/login');

    final queryParam = <String, dynamic>{
      'username': id,
      'password': password,
    };

    final response = await _baseService.dio.post(url, data: queryParam);
    final data = json.decode(response.data);
    final result = AccountModels.fromJson(data);
    return result;
  }
}
