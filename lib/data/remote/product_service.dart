import 'dart:convert';

import 'package:flutter/foundation.dart';

import '../../component/component.dart';
import '../../models/models.dart';
import 'base_service.dart';

class ProductService extends Endpoint {
  final BaseService _baseService;

  ProductService(this._baseService);

  factory ProductService.create() => ProductService(getIt.get());

  static List<ProductModels> _decodeJson(String response) {
    final items = <ProductModels>[];
    final Map<String, dynamic> data = jsonDecode(response);
    final List<dynamic> result = data['products'];
    for (final item in result) {
      final product = ProductModels.fromJson(item);
      items.add(product);
    }
    return items;
  }

  Future<List<ProductModels>> getListProduct() async {
    final url = endpointBaseUrlWithVersion(path: 'products');

    final response = await _baseService.dio.get(url);
    final String data = response.data;
    final result = await compute(_decodeJson, data);
    return result;
  }
}
