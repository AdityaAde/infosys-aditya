// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'product_models.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProductModels _$ProductModelsFromJson(Map<String, dynamic> json) {
  return _ProductModels.fromJson(json);
}

/// @nodoc
mixin _$ProductModels {
  String? get title => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  int? get price => throw _privateConstructorUsedError;
  int? get stock => throw _privateConstructorUsedError;
  String? get thumbnail => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProductModelsCopyWith<ProductModels> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductModelsCopyWith<$Res> {
  factory $ProductModelsCopyWith(
          ProductModels value, $Res Function(ProductModels) then) =
      _$ProductModelsCopyWithImpl<$Res>;
  $Res call(
      {String? title,
      String? description,
      int? price,
      int? stock,
      String? thumbnail});
}

/// @nodoc
class _$ProductModelsCopyWithImpl<$Res>
    implements $ProductModelsCopyWith<$Res> {
  _$ProductModelsCopyWithImpl(this._value, this._then);

  final ProductModels _value;
  // ignore: unused_field
  final $Res Function(ProductModels) _then;

  @override
  $Res call({
    Object? title = freezed,
    Object? description = freezed,
    Object? price = freezed,
    Object? stock = freezed,
    Object? thumbnail = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int?,
      stock: stock == freezed
          ? _value.stock
          : stock // ignore: cast_nullable_to_non_nullable
              as int?,
      thumbnail: thumbnail == freezed
          ? _value.thumbnail
          : thumbnail // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_ProductModelsCopyWith<$Res>
    implements $ProductModelsCopyWith<$Res> {
  factory _$$_ProductModelsCopyWith(
          _$_ProductModels value, $Res Function(_$_ProductModels) then) =
      __$$_ProductModelsCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? title,
      String? description,
      int? price,
      int? stock,
      String? thumbnail});
}

/// @nodoc
class __$$_ProductModelsCopyWithImpl<$Res>
    extends _$ProductModelsCopyWithImpl<$Res>
    implements _$$_ProductModelsCopyWith<$Res> {
  __$$_ProductModelsCopyWithImpl(
      _$_ProductModels _value, $Res Function(_$_ProductModels) _then)
      : super(_value, (v) => _then(v as _$_ProductModels));

  @override
  _$_ProductModels get _value => super._value as _$_ProductModels;

  @override
  $Res call({
    Object? title = freezed,
    Object? description = freezed,
    Object? price = freezed,
    Object? stock = freezed,
    Object? thumbnail = freezed,
  }) {
    return _then(_$_ProductModels(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int?,
      stock: stock == freezed
          ? _value.stock
          : stock // ignore: cast_nullable_to_non_nullable
              as int?,
      thumbnail: thumbnail == freezed
          ? _value.thumbnail
          : thumbnail // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ProductModels implements _ProductModels {
  const _$_ProductModels(
      {this.title, this.description, this.price, this.stock, this.thumbnail});

  factory _$_ProductModels.fromJson(Map<String, dynamic> json) =>
      _$$_ProductModelsFromJson(json);

  @override
  final String? title;
  @override
  final String? description;
  @override
  final int? price;
  @override
  final int? stock;
  @override
  final String? thumbnail;

  @override
  String toString() {
    return 'ProductModels(title: $title, description: $description, price: $price, stock: $stock, thumbnail: $thumbnail)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ProductModels &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality()
                .equals(other.description, description) &&
            const DeepCollectionEquality().equals(other.price, price) &&
            const DeepCollectionEquality().equals(other.stock, stock) &&
            const DeepCollectionEquality().equals(other.thumbnail, thumbnail));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(description),
      const DeepCollectionEquality().hash(price),
      const DeepCollectionEquality().hash(stock),
      const DeepCollectionEquality().hash(thumbnail));

  @JsonKey(ignore: true)
  @override
  _$$_ProductModelsCopyWith<_$_ProductModels> get copyWith =>
      __$$_ProductModelsCopyWithImpl<_$_ProductModels>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ProductModelsToJson(
      this,
    );
  }
}

abstract class _ProductModels implements ProductModels {
  const factory _ProductModels(
      {final String? title,
      final String? description,
      final int? price,
      final int? stock,
      final String? thumbnail}) = _$_ProductModels;

  factory _ProductModels.fromJson(Map<String, dynamic> json) =
      _$_ProductModels.fromJson;

  @override
  String? get title;
  @override
  String? get description;
  @override
  int? get price;
  @override
  int? get stock;
  @override
  String? get thumbnail;
  @override
  @JsonKey(ignore: true)
  _$$_ProductModelsCopyWith<_$_ProductModels> get copyWith =>
      throw _privateConstructorUsedError;
}
