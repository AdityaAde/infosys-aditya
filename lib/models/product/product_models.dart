import 'package:freezed_annotation/freezed_annotation.dart';

part 'product_models.freezed.dart';
part 'product_models.g.dart';

@freezed
class ProductModels with _$ProductModels {
  const factory ProductModels({
    String? title,
    String? description,
    int? price,
    int? stock,
    String? thumbnail,
  }) = _ProductModels;

  factory ProductModels.fromJson(Map<String, dynamic> json) =>
      _$ProductModelsFromJson(json);
}
