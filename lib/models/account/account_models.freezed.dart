// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'account_models.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AccountModels _$AccountModelsFromJson(Map<String, dynamic> json) {
  return _AccountModels.fromJson(json);
}

/// @nodoc
mixin _$AccountModels {
  String? get username => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get image => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AccountModelsCopyWith<AccountModels> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AccountModelsCopyWith<$Res> {
  factory $AccountModelsCopyWith(
          AccountModels value, $Res Function(AccountModels) then) =
      _$AccountModelsCopyWithImpl<$Res>;
  $Res call({String? username, String? email, String? image});
}

/// @nodoc
class _$AccountModelsCopyWithImpl<$Res>
    implements $AccountModelsCopyWith<$Res> {
  _$AccountModelsCopyWithImpl(this._value, this._then);

  final AccountModels _value;
  // ignore: unused_field
  final $Res Function(AccountModels) _then;

  @override
  $Res call({
    Object? username = freezed,
    Object? email = freezed,
    Object? image = freezed,
  }) {
    return _then(_value.copyWith(
      username: username == freezed
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_AccountModelsCopyWith<$Res>
    implements $AccountModelsCopyWith<$Res> {
  factory _$$_AccountModelsCopyWith(
          _$_AccountModels value, $Res Function(_$_AccountModels) then) =
      __$$_AccountModelsCopyWithImpl<$Res>;
  @override
  $Res call({String? username, String? email, String? image});
}

/// @nodoc
class __$$_AccountModelsCopyWithImpl<$Res>
    extends _$AccountModelsCopyWithImpl<$Res>
    implements _$$_AccountModelsCopyWith<$Res> {
  __$$_AccountModelsCopyWithImpl(
      _$_AccountModels _value, $Res Function(_$_AccountModels) _then)
      : super(_value, (v) => _then(v as _$_AccountModels));

  @override
  _$_AccountModels get _value => super._value as _$_AccountModels;

  @override
  $Res call({
    Object? username = freezed,
    Object? email = freezed,
    Object? image = freezed,
  }) {
    return _then(_$_AccountModels(
      username: username == freezed
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AccountModels implements _AccountModels {
  const _$_AccountModels({this.username, this.email, this.image});

  factory _$_AccountModels.fromJson(Map<String, dynamic> json) =>
      _$$_AccountModelsFromJson(json);

  @override
  final String? username;
  @override
  final String? email;
  @override
  final String? image;

  @override
  String toString() {
    return 'AccountModels(username: $username, email: $email, image: $image)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AccountModels &&
            const DeepCollectionEquality().equals(other.username, username) &&
            const DeepCollectionEquality().equals(other.email, email) &&
            const DeepCollectionEquality().equals(other.image, image));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(username),
      const DeepCollectionEquality().hash(email),
      const DeepCollectionEquality().hash(image));

  @JsonKey(ignore: true)
  @override
  _$$_AccountModelsCopyWith<_$_AccountModels> get copyWith =>
      __$$_AccountModelsCopyWithImpl<_$_AccountModels>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AccountModelsToJson(
      this,
    );
  }
}

abstract class _AccountModels implements AccountModels {
  const factory _AccountModels(
      {final String? username,
      final String? email,
      final String? image}) = _$_AccountModels;

  factory _AccountModels.fromJson(Map<String, dynamic> json) =
      _$_AccountModels.fromJson;

  @override
  String? get username;
  @override
  String? get email;
  @override
  String? get image;
  @override
  @JsonKey(ignore: true)
  _$$_AccountModelsCopyWith<_$_AccountModels> get copyWith =>
      throw _privateConstructorUsedError;
}
