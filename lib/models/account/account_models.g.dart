// GENERATED CODE - DO NOT MODIFY BY HAND

// ignore_for_file: non_constant_identifier_names

part of 'account_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AccountModels _$$_AccountModelsFromJson(Map<String, dynamic> json) =>
    _$_AccountModels(
      username: json['username'] as String?,
      email: json['email'] as String?,
      image: json['image'] as String?,
    );

Map<String, dynamic> _$$_AccountModelsToJson(_$_AccountModels instance) =>
    <String, dynamic>{
      'username': instance.username,
      'email': instance.email,
      'image': instance.image,
    };
