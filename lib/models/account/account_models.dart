import 'package:freezed_annotation/freezed_annotation.dart';

part 'account_models.freezed.dart';
part 'account_models.g.dart';

@freezed
class AccountModels with _$AccountModels {
  const factory AccountModels({
    String? username,
    String? email,
    String? image,
  }) = _AccountModels;

  factory AccountModels.fromJson(Map<String, dynamic> json) =>
      _$AccountModelsFromJson(json);
}
