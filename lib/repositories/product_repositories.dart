import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import '../component/component.dart';
import '../data/remote/remote.dart';
import '../models/models.dart';

class ProductRepository {
  final ProductService _productService;

  ProductRepository(this._productService);

  factory ProductRepository.create() => ProductRepository(getIt.get());

  Future<Either<Exception, List<ProductModels>>> getListProduct() async {
    try {
      final result = await _productService.getListProduct();
      return Right(result);
    } on DioError catch (dioError) {
      switch (dioError.type) {
        case DioErrorType.connectTimeout:
        case DioErrorType.receiveTimeout:
        case DioErrorType.sendTimeout:
          return Left(Exception('No Connection'));
        case DioErrorType.response:
          return Left(Exception('Data Parsing err'));
        default:
          return Left(Exception());
      }
    } catch (e) {
      return Left(Exception());
    }
  }
}
