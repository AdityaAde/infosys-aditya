import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import '../component/injector.dart';
import '../data/remote/remote.dart';
import '../models/models.dart';

class LoginRepository {
  final LoginService _loginService;

  LoginRepository(this._loginService);

  factory LoginRepository.create() => LoginRepository(getIt.get());

  Future<Either<Exception, AccountModels>> login(
    String id,
    String password,
  ) async {
    try {
      final result = await _loginService.login(id, password);
      return Right(result);
    } on DioError catch (dioError) {
      switch (dioError.type) {
        case DioErrorType.connectTimeout:
        case DioErrorType.receiveTimeout:
        case DioErrorType.sendTimeout:
          return Left(Exception('No Connection'));
        case DioErrorType.response:
          return Left(Exception('Authentication Err'));
        default:
          return Left(Exception());
      }
    } catch (e) {
      return Left(Exception());
    }
  }
}
